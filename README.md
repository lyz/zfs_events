# zfs_events

This is a small script for logging zfs events in the Loki format to a systemd service.

## Installation

```bash
git clone https://codeberg.org/lyz/zfs_events.git
cd zfs_events
sudo cp zfs_events /usr/bin/ 
sudo cp zfs_events.service /etc/systemd/system/
sudo systemctl start zfs_events
sudo systemctl enable zfs_events
```

## Usage

Assuming that you're sending the `journald` logs to Loki you can use the alerts defined in `zfs_events.yaml`.
## License

GPLv3
